#ifndef BVD_IWINDOW_H
#define BVD_IWINDOW_H

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Window/Event.hpp>

namespace bvd
{
	class iWindow : public sf::Drawable
	{
	public:
		virtual ~iWindow() {}
		
		virtual void setSize(sf::Vector2f size) = 0;
		virtual void setPosition(sf::Vector2f pos) = 0;
		virtual void handleInput(const sf::Event& event) = 0;
	};
}

#endif
