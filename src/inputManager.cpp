#include "inputManager.h" 
#include <iostream>

bvd::InputManager::InputManager()
{
	
}

bool bvd::InputManager::loadBindingsFromFile(const std::string &path)
{
	return true;
}

bvd::Event bvd::InputManager::getEventFromInput(const sf::Event& input)
{
	if (input.type == sf::Event::KeyPressed)
	{
		if (input.key.code == sf::Keyboard::LControl ||
			input.key.code == sf::Keyboard::LAlt ||
			input.key.code == sf::Keyboard::LShift ||
			input.key.code == sf::Keyboard::Menu)
		{
			return applyModifier(input.key.code, true);
		}

		struct bvd::Binding binding;
		binding.modifier = currentModifier;
		binding.key = input.text.unicode;
		bvd::Event value = getEvent(binding);
		if(value == bvd::NOT_BINDED) { return bvd::KEY; }
		return value;
	}
	else if (input.type == sf::Event::KeyReleased)
	{
		if (input.key.code == sf::Keyboard::LControl ||
			input.key.code == sf::Keyboard::LAlt ||
			input.key.code == sf::Keyboard::LShift ||
			input.key.code == sf::Keyboard::LSystem)
		{
			return applyModifier(input.key.code, false);
		}
	}
	
	if (input.type == sf::Event::TextEntered)
	{
		//filter out control keys
		uint32_t unicode = input.text.unicode;
		if (unicode < 0x1B)
		{
			unicode = unicode + 96;
		}
		else if (unicode < 0x20)
		{
			unicode = unicode + 64;
		}
		
		if (unicode < 128)
		{
			if(currentModifier == 0)
			{
				return bvd::TEXT;
			}
			
			struct bvd::Binding binding;
			binding.modifier = currentModifier;
			binding.key = unicode;
			return getEvent(binding);
		}
	}

	return bvd::UNKNOWN;
}

bvd::Event bvd::InputManager::getEvent(const struct bvd::Binding& binding)
{
	bvd::key_map::const_iterator it = keybindings.find(binding);

	if (it != keybindings.end())
	{
		return it->second;
	}
	return bvd::NOT_BINDED;
}

bool bvd::InputManager::setBinding(const struct bvd::Binding& binding, bvd::Event event)
{
	bvd::key_map::iterator it = keybindings.find(binding);

	if (it == keybindings.end())
	{
		keybindings.emplace(binding, event);
	}
	else
	{
		it->second = event;
	}
	return true;
}

bvd::Event bvd::InputManager::applyModifier(sf::Keyboard::Key mod, bool add)
{
	uint8_t mask = 0;
	bvd::Event returnValue = UNKNOWN;
	
	switch(mod)
	{
	case sf::Keyboard::LControl:
		mask = bvd::mask::CTRL;
		returnValue = bvd::CTRL;
		break;

	case sf::Keyboard::LShift:
		mask = bvd::mask::SHIFT;
		returnValue = bvd::SHIFT;
		break;

	case sf::Keyboard::LAlt:
		mask = bvd::mask::ALT;
		returnValue = bvd::ALT;
		break;

	case sf::Keyboard::LSystem:
		mask = bvd::mask::META;
		returnValue = bvd::META;
		break;
		
	default:
		return bvd::UNKNOWN;
		break;
	};

	if (add)
	{
		currentModifier |= mask;
	}
	else
	{
		currentModifier &= ~(mask);
	}
	
	return returnValue;
}
