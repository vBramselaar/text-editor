#ifndef BVD_BUFFER_H
#define BVD_BUFFER_H

#include <string>
#include <list>
#include <fstream>
#include <codecvt>

namespace bvd
{
	class Buffer
	{
	public:
		Buffer();
		bool loadFrom(const std::string& filePath);
		bool save();
		bool saveTo(const std::string& filePath);
		bool insert(wchar_t ch, size_t line, size_t offset);
		bool erase(size_t line, size_t offset);
		int backspaceLine(size_t line);
		bool newLine(size_t line, size_t offset);
		bool getLine(size_t index, std::wstring& output);
		int getLineLength(unsigned int index);
		
		friend class TextArea;

	private:
		std::list<std::wstring> buffer;
		std::string filename;
	};
}

#endif
