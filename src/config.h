#ifndef BVD_CONFIG_H
#define BVD_CONFIG_H

#include <SFML/Graphics/Color.hpp>

#define CURSOR L"\u2588"
#define TEXT_COLOUR sf::Color::White
#define BACKGROUND_COLOUR sf::Color::Black
#define CURSOR_COLOUR sf::Color(100, 100, 100)
#define TEXT_SIZE 20

#endif
