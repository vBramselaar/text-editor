#include "buffer.h"
#include <iostream>

bvd::Buffer::Buffer()
{
	
}

bool bvd::Buffer::loadFrom(const std::string& filePath)
{
	std::wifstream in(filePath, std::ios::in | std::ios::binary);
	//load as utf-8
	in.imbue(std::locale(std::locale(), new std::codecvt_utf8<wchar_t>));
	if (in)
	{
		std::wstring line;
		while(std::getline(in, line))
		{
			buffer.push_back(line);
		}
		
		filename = filePath;
		return true;
	}
	
	return false;
}

bool bvd::Buffer::save()
{
	return saveTo(filename);
}

bool bvd::Buffer::saveTo(const std::string& filePath)
{
	std::wofstream out(filePath, std::ios::out | std::ios::binary);
	for(const std::wstring& line : buffer)
	{
		out << line << std::endl;
	}
	return true;
}



bool bvd::Buffer::insert(wchar_t ch, size_t line, size_t offset)
{
	if (line < buffer.size())
	{
		std::list<std::wstring>::iterator it = buffer.begin();
		std::advance(it, line);

		if (offset < it->length())
		{
			std::wstring text;
			text = ch;
			it->insert(offset, text);
			return true;
		}
		else if (offset == it->length())
		{
			it->push_back(ch);
			return true;
		}
	}
	
	return false;
}

bool bvd::Buffer::erase(size_t line, size_t offset)
{
	if (line < buffer.size())
	{
		std::list<std::wstring>::iterator it = buffer.begin();
		std::advance(it, line);

		if (it->length() == 0)
		{
			return false;
		}
		else if (offset == it->length())
		{
			it->pop_back();
			return true;
		}
		else if (offset < it->length())
		{
			it->erase(offset, 1);
			return true;
		}
	}
	
	return false;
}

int bvd::Buffer::backspaceLine(size_t line)
{
	if (line < buffer.size() && line > 0)
	{
		std::list<std::wstring>::iterator it = buffer.begin();
		std::advance(it, line);

		std::list<std::wstring>::iterator lineBack = buffer.begin();
		std::advance(lineBack, line - 1);

		int newIndex = lineBack->length();
		lineBack->append(*it);
		buffer.erase(it);
		
		return newIndex;
	}

	return -1;
}

bool bvd::Buffer::newLine(size_t line, size_t offset)
{
	if (line < buffer.size())
	{
		std::list<std::wstring>::iterator it = buffer.begin();
		std::advance(it, line);

		std::wstring newLine = it->substr(offset);
		
		it->erase(offset, newLine.length());

		std::advance(it, 1);
		buffer.insert(it, newLine);
		return true;
	}

	return false;
}

bool bvd::Buffer::getLine(size_t index, std::wstring& output)
{
	if (index < buffer.size())
	{
		std::list<std::wstring>::iterator it = buffer.begin();
		std::advance(it, index);

		output = *it;
		return true;
	}

	return false;
}

int bvd::Buffer::getLineLength(unsigned int index)
{
	if (index < buffer.size())
	{
		std::list<std::wstring>::iterator it = buffer.begin();
		std::advance(it, index);

		return it->length();
	}
	
	return -1;
}
