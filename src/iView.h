#ifndef BVD_IVIEW_H
#define BVD_IVIEW_H

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Window/Event.hpp>

namespace bvd
{
	class iView : public sf::Drawable
	{
	public:
		virtual ~iView() {}
		
		virtual void setSize(sf::Vector2f size) = 0;
		virtual void setPosition(sf::Vector2f pos) = 0;
		virtual void handleInput(const sf::Event& event) = 0;
	};
}

#endif
