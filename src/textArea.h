#ifndef BVD_TEXTAREA_H
#define BVD_TEXTAREA_H

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <memory>

#include "iWindow.h"
#include "inputManager.h"
#include "buffer.h"
#include "config.h"

namespace bvd
{
	using buffer_ptr = std::shared_ptr<bvd::Buffer>; 

	
	class TextArea : public bvd::iWindow
	{
	public:
		TextArea(const sf::Font& font, bvd::buffer_ptr buffer);
		void setPosition(sf::Vector2f pos);
		void setSize(sf::Vector2f size);
		void handleInput(const sf::Event& event);
		
	private:
		sf::Text text;
		sf::Text cursor;
		sf::RectangleShape background;
		sf::Vector2<size_t> cursorPos = {0, 0};
		bvd::buffer_ptr mBuffer;
		bvd::InputManager inputManager;

		void updateText();
		void updateCursor();
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};
}

#endif
