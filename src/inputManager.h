#ifndef BVD_INPUTMANAGER_H
#define BVD_INPUTMANAGER_H

#include <string>
#include <unordered_map>
#include <SFML/Window/Event.hpp>
#include "keybinding.h"

namespace bvd
{
	using key_map = std::unordered_map<struct bvd::Binding, bvd::Event, bvd::KeyHash>;
	
	class InputManager
	{
	public:
		InputManager();

		bool loadBindingsFromFile(const std::string& path);
		bvd::Event getEventFromInput(const sf::Event& input);
		bvd::Event getEvent(const struct bvd::Binding& binding);
		bool setBinding(const struct bvd::Binding& binding, bvd::Event event);
		
	private:
		bvd::key_map keybindings;
		uint8_t currentModifier = 0;

		bvd::Event applyModifier(sf::Keyboard::Key mod, bool add);
	};
}

#endif
