#ifndef BVD_KEYBINDING_H
#define BVD_KEYBINDING_H

#include <string>
#include <unordered_map>

namespace bvd
{
	enum Event
	{
		SAVE,
		OPEN_FILE,
		FIND,
		COPY,
		PASTE,
		CUT,
		TEXT,
		KEY,
		CTRL,
		ALT,
		META,
		SHIFT,
		NOT_BINDED,
		UNKNOWN
	};

	namespace mask
	{
        constexpr uint8_t CTRL = 0x01;
        constexpr uint8_t SHIFT = 0x02;
		constexpr uint8_t ALT = 0x04;
		constexpr uint8_t META = 0x08;
	}
	
	struct Binding
	{
		uint8_t modifier = 0;
		uint32_t key = 0;

		bool operator==(struct Binding const& other) const
		{
			return other.modifier == modifier && other.key == key;
		}
	};

	template <typename T>
	inline void hash_combine (std::size_t& seed, const T& val)
	{
		seed ^= std::hash<T>()(val) + 0x9e3779b9 + (seed<<6) + (seed>>2);
	}

	
	struct KeyHash
	{
		std::size_t operator()(struct Binding const& binding) const noexcept
		{
			std::size_t seed = 0;
			hash_combine(seed, binding.modifier);
			hash_combine(seed, binding.key);
			return seed;
		}
	};
}
#endif
