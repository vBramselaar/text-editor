#include "textArea.h"
#include <iostream>

bvd::TextArea::TextArea(const sf::Font& font, buffer_ptr buffer)
{
	text.setFont(font);
	text.setCharacterSize(TEXT_SIZE);
	text.setFillColor(TEXT_COLOUR);

	cursor.setFont(font);
	cursor.setCharacterSize(TEXT_SIZE);
	cursor.setString(CURSOR);
	cursor.setFillColor(CURSOR_COLOUR);
	
	background.setFillColor(BACKGROUND_COLOUR);

	//Bind Ctrl-S
	bvd::Binding binding;
	binding.modifier = bvd::mask::CTRL;
	binding.key = 71;
	inputManager.setBinding(binding, bvd::SAVE);
	
	mBuffer = buffer;
	updateText();
	updateCursor();
}

void bvd::TextArea::setPosition(sf::Vector2f pos)
{
	background.setPosition(pos);
	text.setPosition(pos);
	updateText();
	updateCursor();
}

void bvd::TextArea::setSize(sf::Vector2f size)
{
	background.setSize(size);
	updateText();
	updateCursor();
}

void bvd::TextArea::handleInput(const sf::Event &event)
{
	if(!mBuffer) { return; }

	bvd::Event result = inputManager.getEventFromInput(event);
	
	if (result == bvd::KEY)
	{
		if (event.key.code == sf::Keyboard::Left)
		{
			if(cursorPos.x > 0) { cursorPos.x--; }
			updateCursor();
		}

		if (event.key.code == sf::Keyboard::Right)
		{
			std::wstring line;
			if(mBuffer->getLine(cursorPos.y, line))
			{
				if(line.length() > cursorPos.x)
				{
					cursorPos.x++;
					updateCursor();
				}
			}
		}

		if (event.key.code == sf::Keyboard::Down)
		{
			if (cursorPos.y < mBuffer->buffer.size() - 1)
			{
				cursorPos.y++;
				int length = mBuffer->getLineLength(cursorPos.y);
				if (cursorPos.x > static_cast<size_t>(length)) { cursorPos.x = length; }
				updateCursor();
			}
		}

		if (event.key.code == sf::Keyboard::Up)
		{
			if (cursorPos.y > 0 )
			{
				cursorPos.y--;
				int length = mBuffer->getLineLength(cursorPos.y);
				if (cursorPos.x > static_cast<size_t>(length)) { cursorPos.x = length; }
				updateCursor();
			}
		}

		if (event.key.code == sf::Keyboard::Backspace)
		{
			if (cursorPos.x == 0)
			{
				int newCursorPos = mBuffer->backspaceLine(cursorPos.y);
				if (newCursorPos >= 0)
				{
					cursorPos.y--;
					cursorPos.x = newCursorPos;
					updateText();
					updateCursor();
				}
			}
			else
			{
				if(mBuffer->erase(cursorPos.y, cursorPos.x - 1))
				{
					cursorPos.x--;
					updateText();
					updateCursor();
				}
			}
		}

		if (event.key.code == sf::Keyboard::Enter)
		{
			if (mBuffer->newLine(cursorPos.y, cursorPos.x))
			{
				cursorPos.y++;
				cursorPos.x = 0;
				updateText();
				updateCursor();
			}
		}
	}

	if(result == bvd::NOT_BINDED)
	{
		std::cout << "NOT_BINDED" << std::endl;
	}
	else if(result == bvd::SAVE)
	{
		std::cout << "SAVE" << std::endl;
		mBuffer->save();
	}
	
	else if (result == bvd::TEXT && event.text.unicode != 8 && event.text.unicode != 13)
	{
		if (mBuffer->insert(static_cast<wchar_t>(event.text.unicode), cursorPos.y, cursorPos.x))
		{
			cursorPos.x++;
			updateText();
			updateCursor();
		}
	}
}

void bvd::TextArea::updateText()
{
	text.setString(L"");
	for(const std::wstring& line : mBuffer->buffer)
	{
		text.setString(text.getString() + line + L"\n");
	}
}

void bvd::TextArea::updateCursor()
{
	size_t index = 0;
	std::list<std::wstring>::iterator it = mBuffer->buffer.begin();
	for(size_t i = 0; i < cursorPos.y; i++)
	{
		//+1 for new line character
		index += it->length() + 1;
		std::advance(it, 1);
	}

	index += cursorPos.x;
	
	cursor.setPosition(text.findCharacterPos(index));
}

void bvd::TextArea::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(background, states);
	target.draw(cursor, states);
	target.draw(text, states);
}

 
