#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include "textArea.h"
#include "iWindow.h"

#include <lua.hpp>

sf::Vector2f returnSpacing(const sf::RenderWindow& window)
{
	float width = sf::VideoMode::getDesktopMode().width;
	float height = sf::VideoMode::getDesktopMode().height;

	width = round(width * 0.05);
	height = round(height * 0.05);

	sf::Vector2f spacing;
	if(width > height)
	{
		spacing = {height, height};
	}
	else
	{
		spacing = {width, width};
	}
	
	return spacing;
}

sf::Vector2f returnSize(const sf::RenderWindow& window)
{
	sf::Vector2f spacings = returnSpacing(window);
	float spacing = spacings.y;
	
	return sf::Vector2f(window.getSize().x - (spacing * 2), window.getSize().y - (spacing*2));
}


int main(int argc, char *argv[])
{
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	lua_close(L);

	sf::RenderWindow frame;
    frame.create(sf::VideoMode(800, 600), "My window");
	frame.setFramerateLimit(60);
	
	sf::Font font;
	if (!font.loadFromFile("./resources/fonts/Hack-Regular.ttf"))
	{
		std::cout << "font error" << std::endl;
		return -1;
	}

	bvd::buffer_ptr buffer = std::make_shared<bvd::Buffer>();
	if (!buffer->loadFrom(argv[1]))
	{
		std::cout << "buffer error" << std::endl;
		return -1;
	}

	bvd::iWindow* window = new bvd::TextArea(font, buffer);
	
	sf::Vector2f spacing = returnSpacing(frame);
	window->setPosition(spacing);
	window->setSize(returnSize(frame));

	while (frame.isOpen())
    {
		frame.clear(sf::Color::White);
		
        sf::Event event;
        while (frame.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
			{
                frame.close();
			}

			if (event.type == sf::Event::Resized)
			{
				sf::FloatRect visibleArea(0,0 ,event.size.width, event.size.height);
				frame.setView(sf::View(visibleArea));

				sf::Vector2f spacing = returnSpacing(frame);
				window->setPosition(spacing);
				window->setSize(returnSize(frame));
			}

			window->handleInput(event);
		}
		
		frame.draw(*window);
		frame.display();
	}

	delete window;

	return 0;
}
 
