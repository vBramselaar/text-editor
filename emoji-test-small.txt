1F600                                      ; fully-qualified     # 😀 grinning face
1F601                                      ; fully-qualified     # 😁 beaming face with smiling eyes
1F602                                      ; fully-qualified     # 😂 face with tears of joy
1F923                                      ; fully-qualified     # 🤣 rolling on the floor laughing
1F603                                      ; fully-qualified     # 😃 grinning face with big eyes
1F604                                      ; fully-qualified     # 😄 grinning face with smiling eyes
1F605                                      ; fully-qualified     # 😅 grinning face with sweat
1F606                                      ; fully-qualified     # 😆 grinning squinting face
1F609                                      ; fully-qualified     # 😉 winking face
1F60A                                      ; fully-qualified     # 😊 smiling face with smiling eyes
1F60B                                      ; fully-qualified     # 😋 face savoring food
1F60E                                      ; fully-qualified     # 😎 smiling face with sunglasses
1F60D                                      ; fully-qualified     # 😍 smiling face with heart-eyes
1F618                                      ; fully-qualified     # 😘 face blowing a kiss
1F617                                      ; fully-qualified     # 😗 kissing face
1F619                                      ; fully-qualified     # 😙 kissing face with smiling eyes
1F61A                                      ; fully-qualified     # 😚 kissing face with closed eyes
263A FE0F                                  ; fully-qualified     # ☺️ smiling face
263A                                       ; non-fully-qualified # ☺ smiling face
1F642                                      ; fully-qualified     # 🙂 slightly smiling face
1F917                                      ; fully-qualified     # 🤗 hugging face
1F929                                      ; fully-qualified     # 🤩 star-struck
