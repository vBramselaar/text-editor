CXX:=g++
CXXFLAGS:=-Wall -Wextra -pedantic -std=c++14
RELEASEF:=-O3
DEBUGF:=-ggdb -O0 -DDEBUG_MODE
LUA=lua5.3
LDFLAGS:=-lsfml-graphics -lsfml-window -lsfml-system -l$(LUA)

CPPFLAGS:=-I./src -I/usr/include/$(LUA)

SRC:=$(wildcard src/*.cpp)
OBJ:=$(patsubst %.cpp,%.o, $(SRC))

PRG:=textEditor

.PHONY:all clean

all:debug

release:CXXFLAGS += $(RELEASEF)
release:$(PRG)
	@echo "Release build"

debug:CXXFLAGS += $(DEBUGF) 
debug:$(PRG)
	@echo "Debug build"

clang_complete:
	@echo $(CPPFLAGS) | tr " " "\n" > .clang_complete
	@echo ".clang_complete generated"

$(PRG):$(OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $^ -o $@ $(LDFLAGS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $< -o $@ $(LDFLAGS)

clean:
	rm -rf $(OBJ)
	rm -rf $(PRG)
